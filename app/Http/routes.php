<?php

Route::get('/', function () {
    return view('welcome');
});


Route::get('/send', function() {

	$user = [
		'name' => 'Iman Syaefulloh',
		'comment' => 'Testing'
	];

	\Mail::queue('emails.testing', compact('user'), function ($m) use ($user) {
        $m->to('imansyaefulloh@gmail.com', 'Iman')->subject('Testing Queue');
    });

});

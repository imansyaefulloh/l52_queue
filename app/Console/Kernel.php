<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $path = 'storage/logs/console-jobs.log';
        
        // $schedule->command('inspire')
        //     ->everyMinute()
        //     ->appendOutputTo($path);

        $schedule->command('queue:work database --daemon --sleep=3 --tries=3')
            ->everyMinute()
            ->appendOutputTo($path);;
    }
}
